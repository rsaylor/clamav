#!/bin/bash
i=0;
rm -f /root/infections.txt
cd /var/cpanel/users
for user in *
do
        echo Now running scan for : $user
        echo >>/root/infections.txt
        echo Scan started for $user >>/root/infections.txt
        echo ============================================ >>/root/infections.txt
        /usr/local/cpanel/3rdparty/bin/clamscan -i -r /home/"$user" >>/root/infections.txt
        i=$((i+1));

        # allow the CPU to cool down between runs with a sleep command
        sleep 10;

        # for testing this will stop the process after 5 runs
        #if [[ "$i" -gt 2 ]]; then
        #        mail -s 'Antivirus Report' robert.saylor@techlinksolutions.com < /root/infections.txt
        #        echo "Exit..."
        #        exit 1
        #fi
done
mail -s 'Antivirus Report' robert.saylor@techlinksolutions.com < /root/infections.txt
